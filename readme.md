## Microservices repository
 

### Etl to Transit

**This ms uploads data into transit table.**

***Run it by tiping:***

> cd ./etl_to_transit
> npm start
> Go to http://localhost:8080/run


### Split Transit to Tables

**This ms splits the data from transit table into resources and trips tables.**

***Run it by tiping:***

> cd ./split_transit_to_tables
> npm start
> Go to http://localhost:8081/run


### Update Resource Trip data

**This ms updates the trip data from trips table. The columns that updates are:**
- resource_status
- current_trip_id
- current_trip_name
- current_trip_start_time
- current_trip_end_time
- next_trip_id
- next_trip_name

***Run it by tiping:***

> cd ./update_resource_trip_data
> npm start
> Go to http://localhost:8082/run


### Update Location info Ituran

**This ms inserts data into location_info table from Ituran. It also adds new resource with type car (if plate number is not found), or updates resource location by columns:**

- latitude
- longitude
- position_last_update
- position_source

***Run it by tiping:***

> cd ./update_location_info_ituran
> npm start
> Go to http://localhost:8083/run

