const express = require('express');
const path = require('path');
const axios = require('axios');
const cron = require('node-cron');
const now = new Date();
const fs = require('fs');

const db = require("./models");
const transitService = require('./services/transitService')(db);
const tripService = require('./services/tripsService')(db);
const resourcesService = require('./services/resourcesService')(db);

const port = 8081; //process.argv.slice(2)[0];
const executeMinutesParam = process.argv.slice(2)[0];
let fetchDateParam = process.argv.slice(2)[1];
const app = express();

console.log();

if (!executeMinutesParam) {
  // node server.js 8081 4 // Every 4 minutes
  console.log(`Warning: Cron will NOT be executed. You can run this with get request to http://localhost:${port}/run. If you want to start the cron type: node [file] [executeMinutesParam=1/15]`);
  console.log();
}

if (!fetchDateParam) {
  // node server.js 8081 1 2020-07-20 // Every 1 minute will get date for 2020-07-20
  fetchDateParam = (now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate());
  console.log(`Warning: Date param not passed. You will get records for today ${fetchDateParam}. You can change the date, by passing date parameter to the get request: ?date=2020-07-20. Or if are starting the cron, pass date by taping: node [file] [executeMinutesParam=1/15] [fetchDateParam=2020-07-20]`);
  console.log();
}

const splitTransitData = async () => {
  const transitData = await transitService.getAllTransit({ data_splitted: 0 }); // car_number: 7290352 // TEMP

  for (let k = 0; k < Object.values(transitData).length; k++) {
    let transit = transitData[k]

    // Insert in resource if there is not existing the current car_number with current driver_code. 
    if (transit.car_number) {
      const resourceExisting = await resourcesService.isResourceExisting(transit.car_number, transit.driver_code);

      let insertedResourceId = 0;
      if (resourceExisting === null) {
        let addResource = setResourceData(transit);
        addResource.resource_type = 'car';
        if (transit.driver_code) {
          addResource.resource_type = 'car, driver';
        }
        await resourcesService.addResource(addResource).then(result => insertedResourceId = result.id);
      } else {
        insertedResourceId = resourceExisting.id;
      }

      // Insert in trips the car with the different times (start and end)
      let addTrip = setTripData(transit);
      addTrip.resource_id = insertedResourceId;
      addTrip.company_id = 1;
      await tripService.addTrip(addTrip);

      // Update the trips count in resources
      await resourcesService.incrementResource(['total_trips'], { by: 1, where: { id: insertedResourceId } })

    } else if (transit.driver_code) {

      // Insert in resource with type driver
      const resourceExisting = await resourcesService.isResourceExisting(transit.car_number, transit.driver_code);

      if (resourceExisting === null) {
        let addResourceDriver = setResourceData(transit);
        addResourceDriver.resource_type = 'driver';
        await resourcesService.addResource(addResourceDriver);
      }

    }

  }

  // Update all transit records we got as data_splitted
  for (let j = 0; j < Object.values(transitData).length; j++) {
    transitData[j].update({
      data_splitted: 1,
    })
  }

  console.log(`Transit data should be splitted`)

}

function setTripData(transit) {
  return {
    "company_id": transit.company_id,
    "client_code": transit.client_code,
    "start_time": transit.order_start_time,
    "end_time": transit.order_end_time,
    "line_code": transit.line_code,
    "course_code": transit.course_code,
    "line_description": transit.line_description,
    "stations": transit.stations,
  };
}

function setResourceData(transit) {
  return {
    "company_id": transit.company_id,
    "company_code": 0, // REQUIRED
    // "resource_type": "test", // REQUIRED - This is set after this function call
    "line_date": transit.line_date, // REQUIRED
    "is_active": transit.is_active,
    "line_code": transit.line_code,
    "order_start_time": transit.order_start_time, // REQUIRED
    "order_end_time": transit.order_start_time,
    "order_car_type": transit.order_car_type,
    "line_description": transit.line_description,
    "line_status": transit.line_status,
    "line_type": transit.line_type,
    "course_code": transit.course_code,
    "driver_code": transit.driver_code,
    "car_id": transit.car_id,
    "car_number": transit.car_number,
    "car_code": transit.car_code,
    "client_id": 0, // REQUIRED
    "client_code": transit.client_code,
    "stations": transit.stations,
    // "current_trip_id": null, // This will be updated later with the sql 1 min req
    // "current_trip_name": null, // This will be updated later with the sql 1 min req
    // "current_trip_start_time": null, // This will be updated later with the sql 1 min req
    // "current_trip_end_time": null, // This will be updated later with the sql 1 min req
    // "total_trips": 0, // This is updated later
    "data_source": "split_transit_to_tables", // REQUIRED
    // "last_update_date_time": "2021-10-20T09:33:00.000Z",
  }
}

let cronTask;

// Cron execution settings
if (executeMinutesParam) {
  let executeMinute = parseInt(executeMinutesParam);
  let cronExecuteFormat = '';

  cronExecuteFormat = `*/${executeMinute} * * * *`; // EVERY X MINUTES
  console.log(`Cron will be executed every ${executeMinute} MINUTES`)

  cronTask = cron.schedule(cronExecuteFormat, function () {
    console.log('Cron executed - ', new Date())
    splitTransitData();
  });
}

app.get('/run', (req, res) => {
  res.json(splitTransitData());
})

app.use('/img', express.static(path.join(__dirname, 'img')));

// db.sequelize.sync({ force: true }).then(function () { // drops the tables
//   // db.sequelize.sync({ alter: true }).then(function () { 
//   console.log("Successfully altered DB");
// });

console.log()
console.log(`Microservice splitting Transit data to resources and trips on port ${port}`);
app.listen(port);
