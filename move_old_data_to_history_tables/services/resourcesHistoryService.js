const { Op } = require("sequelize");

module.exports = (function (db) {
    return {
        addResource,
        updateResource,
        incrementResource,
        getAllResources,
        getResource,
        isResourceExisting,
        addBulkResources,
        destroyResources,
    };

    function addResource(data) {
        return db.resources_history.create(data)
    }

    function updateResource(data, where) {
        return db.resources_history.update(data, { where: where })
    }

    function incrementResource(column, options) {
        return db.resources_history.increment(column, options);
    }

    function getAllResources(where) {
        return db.resources_history.findAll({ where: where });
    }

    function getResource(where) {
        return db.resources_history.findOne({ where: where });
    }

    function isResourceExisting(car_number = null, driver_code = null) {
        let where = { car_number: car_number };

        if (car_number) {
            where = { car_number: car_number };
        }
        if (driver_code) {
            where = { driver_code: driver_code };
        }

        if (car_number && driver_code) {
            where = { car_number: car_number, driver_code: driver_code };
            // where = {
            //     [Op.and]: [{ car_number: car_number }, { driver_code: driver_code }]
            // }
        }

        // transit
        return db.resources_history.findOne({ where: where }); // Do not change it to (where)
    }

    function addBulkResources(data) {
        return db.resources_history.bulkCreate(data)
    }

    function destroyResources(where) {
        return db.resources_history.destroy({ where: where });
    }
    
})
