const { Op } = require("sequelize");
var moment = require('moment');
const runConfig = require('../config.json')

module.exports = (function (db) {
    return {
        getAllLocationInfo,
        destroyLocationInfo,
        addBulkLocationInfo
    };

    function getAllLocationInfo(where) {
        return db.location_info.findAll({ where: where });
    }
    
    function destroyLocationInfo(where) {
        return db.location_info.destroy({ where: where });
    }

    function addBulkLocationInfo(data) {
        // db.Sequelize.Op
        return db.location_info.bulkCreate(data)
    }

})
