const { Op } = require("sequelize");

module.exports = (function (db) {
    return {
        addTrip,
        addBulkTrips,
        destroyTrips,
        getAllTrips,
    };

    function getAllTrips(where) {
        return db.trips.findAll({ where: where });
    }

    function addTrip(data) {
        return db.trips.create(data)
    }
    
    function addBulkTrips(data) {
        return db.trips.bulkCreate(data)
    }

    function destroyTrips(where) {
        return db.trips.destroy({ where: where });
    }

})
