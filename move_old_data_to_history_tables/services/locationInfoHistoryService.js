module.exports = (function (db) {
    return {
        getAllLocationHistoryInfo,
        addBulkLocationHistoryInfo
    };

    function getAllLocationHistoryInfo(where) {
        return db.location_info_history.findAll({ where: where });
    }

    function addBulkLocationHistoryInfo(data) {
        // db.Sequelize.Op
        return db.location_info_history.bulkCreate(data)
    }

})
