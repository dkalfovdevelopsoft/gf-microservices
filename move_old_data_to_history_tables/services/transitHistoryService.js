module.exports = (function (db) {
    return {
        getAllTransit,
        addBulkTransit,
        destroyTransit,
    };

    function getAllTransit(where) {
        return db.transit_history.findAll({ where: where });
    }

    function addBulkTransit(data) {
        // db.Sequelize.Op
        return db.transit_history.bulkCreate(data)
    }

    function destroyTransit(where) {
        return db.transit_history.destroy({ where: where });
    }

})
