module.exports = (function (db) {
    return {
        getAllTransit,
        addBulkTransit,
        destroyTransit,
    };

    function getAllTransit(where) {
        return db.transit.findAll({ where: where });
    }

    function addBulkTransit(data) {
        // db.Sequelize.Op
        return db.transit.bulkCreate(data)
    }

    function destroyTransit(where) {
        return db.transit.destroy({ where: where });
    }

})
