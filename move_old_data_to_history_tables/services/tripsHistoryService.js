const { Op } = require("sequelize");

module.exports = (function (db) {
    return {
        addTrip,
        addBulkTrips,
        destroyTrips,
        getAllTrips,
    };

    function getAllTrips(where) {
        return db.trips_history.findAll({ where: where });
    }

    function addTrip(data) {
        return db.trips_history.create(data)
    }
    
    function addBulkTrips(data) {
        return db.trips_history.bulkCreate(data)
    }

    function destroyTrips(where) {
        return db.trips_history.destroy({ where: where });
    }

})
