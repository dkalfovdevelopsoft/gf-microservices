const express = require('express');
const path = require('path');
const axios = require('axios');
const cron = require('node-cron');
const now = new Date();
const fs = require('fs');
const FormData = require('form-data');
var convert = require('xml-js');
const runConfig = require('./config.json');
const db = require("./models");
const resourcesService = require('./services/resourcesService')(db);
const resourcesHistoryService = require('./services/resourcesHistoryService')(db);

const tripsService = require('./services/tripsService')(db);
const tripsHistoryService = require('./services/tripsHistoryService')(db);

const transitService = require('./services/transitService')(db);
const transitHistoryService = require('./services/transitHistoryService')(db);

const locationInfoService = require('./services/locationInfoService')(db);
const locationInfoHistoryService = require('./services/locationInfoHistoryService')(db);
var moment = require('moment');
const { Op } = require("sequelize");

const port = 8084; //process.argv.slice(2)[0];
const executeMinutesParam = process.argv.slice(2)[0];
let fetchDateParam = process.argv.slice(2)[1];
const app = express();

console.log();

if (!executeMinutesParam) {
  // node server.js 8081 4 // Every 4 minutes
  console.log(`Warning: Cron will NOT be executed. You can run this with get request to http://localhost:${port}/run. If you want to start the cron type: node [file] [executeMinutesParam=1/15]`);
  console.log();
}

if (!fetchDateParam) {
  // node server.js 8081 1 2020-07-20 // Every 1 minute will get date for 2020-07-20
  fetchDateParam = (now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate());
  console.log(`Warning: Date param not passed. You will get records for today ${fetchDateParam}. You can change the date, by passing date parameter to the get request: ?date=2020-07-20. Or if are starting the cron, pass date by taping: node [file] [executeMinutesParam=1/15] [fetchDateParam=2020-07-20]`);
  console.log();
}

const getRunConfigWhere = () => (
  {
    createdAt: {
      [Op.gt]: moment.utc().subtract(runConfig.history_delay_hours, 'h')
    }
  }
)

const moveLocationsToHistory = async () => {
  let runConfigWhere = getRunConfigWhere();

  // Get all records according to the config.json where about history_delay_hours
  let dataToMove = await locationInfoService.getAllLocationInfo(runConfigWhere);

  let historyData = [];
  for (let i = 0; i < dataToMove.length; i++) {
    const { id, createdAt, updatedAt, ...realDataToMove } = dataToMove[i].dataValues; // remove id, createdAt and updatedAt
    // fs.writeFileSync("./resp.json", JSON.stringify(realDataToMove));
    historyData.push(realDataToMove)
  }

  // Insert into history table
  await locationInfoHistoryService.addBulkLocationHistoryInfo(historyData);

  // Drop from the real table
  await locationInfoService.destroyLocationInfo(runConfigWhere);
}

const moveResourcesToHistory = async () => {
  let runConfigWhere = getRunConfigWhere();

  // Get all records according to the config.json where about history_delay_hours
  let dataToMove = await resourcesService.getAllResources(runConfigWhere);

  let historyData = [];
  for (let i = 0; i < dataToMove.length; i++) {
    const { id, createdAt, updatedAt, ...realDataToMove } = dataToMove[i].dataValues; // remove id, createdAt and updatedAt
    // fs.writeFileSync("./resp.json", JSON.stringify(realDataToMove));
    historyData.push(realDataToMove)
  }

  // Insert into history table
  await resourcesHistoryService.addBulkResources(historyData);

  // Drop from the real table
  await resourcesService.destroyResources(runConfigWhere);
}

const moveTripsToHistory = async () => {
  let runConfigWhere = getRunConfigWhere();

  // Get all records according to the config.json where about history_delay_hours
  let dataToMove = await tripsService.getAllTrips(runConfigWhere);

  let historyData = [];
  for (let i = 0; i < dataToMove.length; i++) {
    const { id, createdAt, updatedAt, ...realDataToMove } = dataToMove[i].dataValues; // remove id, createdAt and updatedAt
    // fs.writeFileSync("./resp.json", JSON.stringify(realDataToMove));
    historyData.push(realDataToMove)
  }

  // Insert into history table
  await tripsHistoryService.addBulkTrips(historyData);

  // Drop from the real table
  await tripsService.destroyTrips(runConfigWhere);
}

const moveTransitToHistory = async () => {
  let runConfigWhere = getRunConfigWhere();

  // Get all records according to the config.json where about history_delay_hours
  let dataToMove = await transitService.getAllTransit(runConfigWhere);

  let historyData = [];
  for (let i = 0; i < dataToMove.length; i++) {
    const { id, createdAt, updatedAt, ...realDataToMove } = dataToMove[i].dataValues; // remove id, createdAt and updatedAt
    // fs.writeFileSync("./resp.json", JSON.stringify(realDataToMove));
    historyData.push(realDataToMove)
  }

  // Insert into history table
  await transitHistoryService.addBulkTransit(historyData);

  // Drop from the real table
  await transitService.destroyTransit(runConfigWhere);
}

let cronTask;

// Cron execution settings
if (executeMinutesParam) {
  let executeMinute = parseInt(executeMinutesParam);
  let cronExecuteFormat = '';

  cronExecuteFormat = `*/${executeMinute} * * * *`; // EVERY X MINUTES
  console.log(`Cron will be executed every ${executeMinute} MINUTES`)

  cronTask = cron.schedule(cronExecuteFormat, async function () {
    console.log('Cron executed - ', new Date())
    await moveLocationsToHistory();
    await moveResourcesToHistory();
    await moveTripsToHistory();
    await moveTransitToHistory();
  });
}

app.get('/run', async (req, res) => {

  await moveLocationsToHistory();
  await moveResourcesToHistory();
  await moveTripsToHistory();
  await moveTransitToHistory();

  res.json('Data moved');
})

app.use('/img', express.static(path.join(__dirname, 'img')));

// db.sequelize.sync({ force: true }).then(function () { // drops the tables
//   // db.sequelize.sync({ alter: true }).then(function () { 
//   console.log("Successfully altered DB");
// });

console.log()
console.log(`Microservice splitting Transit data to resources and trips on port ${port}`);
app.listen(port);
