const express = require('express');
const path = require('path');
const axios = require('axios');
const cron = require('node-cron');
const now = new Date();

const db = require("./models");
const transitService = require('./services/transitService')(db);

const port = 8080; //process.argv.slice(2)[0];
const executeWhenParam = process.argv.slice(2)[0];
const executeHourParam = process.argv.slice(2)[1];
let fetchDateParam = process.argv.slice(2)[2];
const app = express();

if (!executeWhenParam || !executeHourParam) {
  // node server.js 8081 at 12:21 // Every day at 12:21
  // node server.js 8081 every 4 // Every 4 hours
  console.log();
  console.log(`Warning: Cron will NOT be executed. You can run this with get request to http://localhost:${port}/run. If you want to start the cron type: node [file] [executeWhenParam=every/at] [executeHourParam=21/23:30]`);
}

if (!fetchDateParam) {
  fetchDateParam = (now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate());
  console.log();
  console.log(`Warning: Date param not passed. You will get records for today ${fetchDateParam}. You can change the date, by passing date parameter to the get request: ?date=2020-07-20. Or if are starting the cron, pass date by taping: node [file] [executeWhenParam=every/at] [executeHourParam=21/23:30] [fetchDateParam=2020-07-20]`);
}


const addUniqueTransitData = (dateParam = null) => {
  // TODO: Here must map the companies with their urls
  let dateToGet = dateParam || fetchDateParam;
  
  axios.get(`http://147.235.178.170:19828/api/Transits/Transit?forDate=${dateToGet}`).then(resp => {

    // Insert only records with driver_id OR car_id
    let respData = resp.data.filter(d => d.driver_code || d.car_id);

    // // TEMP - 1, companyId
    // respData.forEach(e => {
    //   e.companyId = 1;
    // });

    // Add model and insertion
    transitService.addBulkTransit(resp.data)
      .then(res => {
        console.log('Added Transit')
        // console.log(res)
        // res.json(res)
      }).catch(error => {
        console.log('Error adding Transit', error) //TEMP companyId 1
        // console.log(error)
        // res.json(error)
      });

  }).catch(error => {
    console.log('Error getting Transit !', error)
    // console.log(error)
    // res.json(error)
  });
}

let cronTask;

// Cron execution settings
if (executeWhenParam && executeHourParam) {
  let executeHour = parseInt(executeHourParam);
  let executeMinute = (executeHourParam.indexOf(':') != -1) ? (parseInt(executeHourParam.split(':')[1])) : 0;
  let cronExecuteFormat = '';

  if (executeWhenParam == 'at') {
    cronExecuteFormat = `${executeMinute} ${executeHour} * * *`;
    console.log(`Cron will be executed every day at ${executeHour}:${executeMinute}`)
  } else if (executeWhenParam == 'every') {
    cronExecuteFormat = `0 */${executeHour} * * *`;
    // cronExecuteFormat = `*/${executeHour} * * * *`; // EVERY MINUTE with passed hour - FOR TEST !!!
    console.log(`Cron will be executed every ${executeHour} HOURS`)
  } else {
    console.log('ERROR: executeWhenParam not passed. It must be "at" or "every"');
    return;
  }

  cronTask = cron.schedule(cronExecuteFormat, function () {
    console.log('Cron executed - ', new Date())
    addUniqueTransitData();
  });
}

app.get('/run', (req, res) => {
  res.json(addUniqueTransitData(req.query.date));
})

app.use('/img', express.static(path.join(__dirname, 'img')));

// db.sequelize.sync({ force: true }).then(function () { // drops the tables
// // db.sequelize.sync({ alter: true }).then(function () { 
//   console.log("Successfully altered DB");
// });

console.log();
console.log(`Microservice handling ETL to Transit on port ${port}`);
app.listen(port);
