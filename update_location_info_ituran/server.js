const express = require('express');
const path = require('path');
const axios = require('axios');
const cron = require('node-cron');
const now = new Date();
const fs = require('fs');
const FormData = require('form-data');
var convert = require('xml-js');

const db = require("./models");
const resourcesService = require('./services/resourcesService')(db);
const locationInfoService = require('./services/locationInfoService')(db);

const port = 8083; //process.argv.slice(2)[0];
const executeSecondssParam = process.argv.slice(2)[0];
let fetchDateParam = process.argv.slice(2)[1];
const app = express();

console.log();

if (!executeSecondssParam) {
  // node server.js 8081 4 // Every 4 seconds
  console.log(`Warning: Cron will NOT be executed. You can run this with get request to http://localhost:${port}/run. If you want to start the cron type: node [file] [executeSecondssParam=1/15]`);
  console.log();
}

if (!fetchDateParam) {
  // node server.js 8081 1 2020-07-20 // Every 1 second will get date for 2020-07-20
  fetchDateParam = (now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate());
  console.log(`Warning: Date param not passed. You will get records for today ${fetchDateParam}. You can change the date, by passing date parameter to the get request: ?date=2020-07-20. Or if are starting the cron, pass date by taping: node [file] [executeSecondssParam=1/15] [fetchDateParam=2020-07-20]`);
  console.log();
}

const updateLocationFromIturan = async () => {
  let insertLocationInfoData = [];
  await axios.get('https://www.ituran.com/ituranwebservice3/Service3.asmx/GetAllPlatformsData?UserName=matan10&Password=matan5555&ShowAreas=&ShowStatuses=&ShowMileageInMeters=&ShowDriver=')
    .then(async resp => {

      // Parse the xml to json
      let data = JSON.parse(
        convert.xml2json(resp.data, { compact: true, spaces: 2 })
      );

      if (data && data.ServiceAllPlatformData && data.ServiceAllPlatformData.VehList && data.ServiceAllPlatformData.VehList.ServicePlatformData) {
        data = data.ServiceAllPlatformData.VehList.ServicePlatformData;
      } else {
        data = [];
      }

      for (let i = 0; i < data.length; i++) {
        insertLocationInfoData.push({
          source: 'ituran',
          plate_number: data[i].Plate._text,
          last_location_time: data[i].LastLocationTime._text,
          last_latitude: data[i].LastLatitude._text,
          last_longitude: data[i].LastLongitude._text,
          current_driver_id: data[i].currentDriverId._text,
          platfrom_id: data[i].PlatfromId._text,
          UAID: data[i].UAID._text,
        })
      }

      // Add all the records to the db
      await locationInfoService.addBulkLocationInfo(insertLocationInfoData)
        .then(res => {
          console.log('Added Location Info')
          // console.log(res)
          // res.json(res)
        }).catch(error => {
          console.log('Error adding Location Info', error)
          // console.log(error)
          // res.json(error)
        });


    }).catch(error => {
      console.log('Error getting ITURAN data !')
      // console.log(error)
      // res.json(error)
    });

}

const updateResourcesLocations = async () => {

  const locationsData = await locationInfoService.getAllLocationInfo({ is_row_updated: 0 });

  for (let i = 0; i < locationsData.length; i++) {
    let location = locationsData[i];

    let resourceExisting = await resourcesService.getResource({ car_number: location.plate_number })

    if (resourceExisting === null) {
      // insert resource with this plate number
      // insert driver id

      resourcesService.addResource({
        latitude: location.last_latitude,
        longitude: location.last_longitude,
        position_last_update: location.last_location_time,
        position_source: location.source,
        car_number: location.plate_number,
        driver_id: location.current_driver_id,
        resource_type: 'car',
        // resource_type - required
        // line_date - required
        // order_start_time - required
        // client_id - required
        // data_source - required
      })

    } else {

      //  update coordinates by car plate number
      // If ituran last update location data is > that last update location data => then update the location in resources
      if (new Date(location.last_location_time).getTime() > new Date(resourceExisting.position_last_update).getTime()) {
        await resourceExisting.update(
          {
            latitude: location.last_latitude,
            longitude: location.last_longitude,
            position_last_update: location.last_location_time,
            position_source: location.source
          }
        )
      }
      
    }
  }


  // Update all locations records we got as data_splitted
  for (let j = 0; j < locationsData.length; j++) {
    locationsData[j].update({
      is_row_updated: 1,
    })
  }

}


let cronTask;

// Cron execution settings
if (executeSecondssParam) {
  let executeSeconds = parseInt(executeSecondssParam);
  let cronExecuteFormat = '';

  cronExecuteFormat = `*/${executeSeconds} * * * * *`; // EVERY X SECONDS
  console.log(`Cron will be executed every ${executeSeconds} SECONDS`)

  cronTask = cron.schedule(cronExecuteFormat, async function () {
    console.log('Cron executed - ', new Date())
    await updateLocationFromIturan();
    await updateResourcesLocations();
  });
}

app.get('/run', async (req, res) => {
  await updateLocationFromIturan();
  await updateResourcesLocations();
  res.json('end');
})

app.use('/img', express.static(path.join(__dirname, 'img')));

// db.sequelize.sync({ force: true }).then(function () { // drops the tables
//   // db.sequelize.sync({ alter: true }).then(function () { 
//   console.log("Successfully altered DB");
// });

console.log()
console.log(`Microservice splitting Transit data to resources and trips on port ${port}`);
app.listen(port);
