module.exports = (function (db) {
    return {
        getAllLocationInfo,
        addBulkLocationInfo
    };

    function getAllLocationInfo(where) {
        return db.location_info.findAll({ where: where });
    }

    function addBulkLocationInfo(data) {
        // db.Sequelize.Op
        return db.location_info.bulkCreate(data)
    }

})
