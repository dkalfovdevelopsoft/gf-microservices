const express = require('express');
const path = require('path');
const axios = require('axios');
const cron = require('node-cron');

const fs = require('fs');
const { QueryTypes } = require('sequelize');
var moment = require('moment');
var utcMoment = moment.utc();

const now = new Date();
// const nowGtm = new Date().toGMTString();
const nowUtc = utcMoment.format();

const db = require("./models");
const transitService = require('./services/transitService')(db);
const tripService = require('./services/tripsService')(db);
const resourcesService = require('./services/resourcesService')(db);

const port = 8082; //process.argv.slice(2)[0];
const executeMinutesParam = process.argv.slice(2)[0];
const app = express();

console.log();

const MINS_BACK_INTERVAL_FOR_STATUS = 5;
const MINS_FORWARD_INTERVAL_FOR_STATUS = 10;

if (!executeMinutesParam) {
  // node server.js 8081 4 // Every 4 minutes
  console.log(`Warning: Cron will NOT be executed. You can run this with get request to http://localhost:${port}/run. If you want to start the cron type: node [file] [executeMinutesParam=1/15]`);
  console.log();
}

// ще работим с utc 
// Ако е driver този ресур, за сега не му пипаме статус!, но ще имаме списък с коли и ще има шоф с номер на кола и тогава ресур типа ще е car and driver
// проблеми с времето 
// в конфиг ще пазим локалното време. но ще е вс в utc
// company_id
// ще има таск - за зоната и да филтърва - 
// ще се пази в локал сториджа зоната, НО може и да променя зоната която да гледа
// АКО НЯМА ЗОНА НЕ СЕ ВРЪЩА ДАТА. 
// фронт енд ще пасва с jwt-зона

const appendLeadingZeroes = (n) => (n <= 9 ? "0" + n : n)

const calculcateHourWithMinsMore = (splittedEndTime, mins) => {
  let timeHour = parseInt(splittedEndTime[0]);
  let timeMinutes = parseInt(splittedEndTime[1]) + parseInt(mins);
  let timeSeconds = parseInt(splittedEndTime[2]);

  if (timeMinutes > 60) {
    timeHour++;
    timeMinutes -= 60;
  }

  return appendLeadingZeroes(timeHour) + ':' + appendLeadingZeroes(timeMinutes) + ':' + appendLeadingZeroes(timeSeconds);
}

const calculcateHourWithMinsLess = (splittedEndTime, mins) => {
  let timeHour = parseInt(splittedEndTime[0]);
  let timeMinutes = parseInt(splittedEndTime[1]) - parseInt(mins);
  let timeSeconds = parseInt(splittedEndTime[2]);

  if (timeMinutes < 0) {
    timeHour--;
    timeMinutes += 60;
  }

  return appendLeadingZeroes(timeHour) + ':' + appendLeadingZeroes(timeMinutes) + ':' + appendLeadingZeroes(timeSeconds);
}

const updateEachTripStatusInTripTable = async (t, statusResource, compareStartTime, compareEndTime) => {
  // Update each trip - trip_status in trip table
  let updateCurrTripStatusTripTable;

  if (!statusResource) {
    // Curr time before min time
    if (compareStartTime == -1) {
      updateCurrTripStatusTripTable = 'not_active';
    }
    // Curr time after max time
    if (compareEndTime == 1) {
      updateCurrTripStatusTripTable = 'done';
    }
  } else {
    updateCurrTripStatusTripTable = statusResource;
  }

  await tripService.updateTrip({ trip_status: updateCurrTripStatusTripTable }, { id: t.id })

}

const updateResourceTripTimes = async () => {

  // const resources = await db.sequelize.query("call get_resources()", { type: QueryTypes.SELECT });
  const resources = await resourcesService.getAllResources();

  const currTime = appendLeadingZeroes(now.getUTCHours()) + ':' + appendLeadingZeroes(now.getUTCMinutes()) + ':' + appendLeadingZeroes(now.getUTCSeconds());
  console.log(`NOW UTF: ${currTime}`)

  for (let i = 0; i < Object.values(resources).length; i++) {
    let r = resources[i];

    // const tripsForThatResource = await db.sequelize.query(`call get_trips_for_resource_id(${r.id})`, { type: QueryTypes.SELECT });

    let tripTimes = await tripService.getTripTimesForResourceId(r.id);
    if (Object.values(tripTimes).length > 0) {

      let sortedTripsByStartTime = JSON.parse(JSON.stringify(tripTimes)); // clones it, do not do it "= tripTimes", because it create reference and will not be working: https://stackoverflow.com/a/38844778/15220907
      let sortedTripsByEndTime = JSON.parse(JSON.stringify(tripTimes)) // clones it, do not do it "= tripTimes", because it create reference and will not be working: https://stackoverflow.com/a/38844778/15220907

      sortedTripsByStartTime.sort(function (a, b) {
        return a.start_time.localeCompare(b.start_time);
      });

      sortedTripsByEndTime.sort(function (a, b) {
        return a.end_time.localeCompare(b.end_time);
      });

      // let currTime = '06:30:00'; //TEMP - active
      // let currTime = '06:21:00'; //TEMP - expected
      // let currTime = '07:15:00'; //TEMP - finish
      // let currTime = '05:30:00'; //TEMP - not_active
      // let currTime = '17:30:00'; //TEMP - done
      // let currTime = '16:44:00'; //TEMP - active
      // let currTime = '16:57:00'; // TEMP - finish
      // let currTime = '07:36:00'; // TEMP - finish
      // fs.writeFileSync("./temp.json", JSON.stringify(sortedTripsByStartTime));

      let updateStatusResource, setUpdateStatusResource;

      let updateCurrentTripId,
        updateCurrentTripName,
        updateCurrentTripStartTime,
        updateCurrentTripEndTime,
        updateNextTripId,
        updateNextTripName,
        currentTripIndex = -1,
        nextTripIndex = -1;

      for (let k = 0; k < Object.values(sortedTripsByStartTime).length; k++) {
        let t = sortedTripsByStartTime[k];
        let compareStartTime = currTime.localeCompare(t.start_time);
        let compareEndTime = currTime.localeCompare(t.end_time);
        // console.log(`COMPARING for ${t.start_time} < ${currTime} > ${t.end_time} - ${k} `)

        let compareStartTimeExtraMins, compareEndTimeExtraMins;

        if ((t.start_time).indexOf(':') != -1 && (t.end_time).indexOf(':') != -1) {
          let splittedStartTime = (t.start_time).split(':');
          let startTimeMinsBack;
          startTimeMinsBack = calculcateHourWithMinsLess(splittedStartTime, MINS_BACK_INTERVAL_FOR_STATUS)

          let splittedEndTime = (t.end_time).split(':');
          let endTimeMinsForward;
          endTimeMinsForward = calculcateHourWithMinsMore(splittedEndTime, MINS_FORWARD_INTERVAL_FOR_STATUS)

          compareStartTimeExtraMins = currTime.localeCompare(startTimeMinsBack);
          compareEndTimeExtraMins = currTime.localeCompare(endTimeMinsForward);
        }

        if (compareStartTime == 1 && compareEndTime == -1) { // now > startTime && now < endTime
          updateStatusResource = 'active';
          currentTripIndex = k;
          nextTripIndex = k + 1;
          await updateEachTripStatusInTripTable(t, updateStatusResource, compareStartTime, compareEndTime); // Update trip status in trip table - This is needed because of the break
          break;
        } else if (compareStartTimeExtraMins == 1 && compareEndTime == -1) {
          setUpdateStatusResource = 'expected';
          // currentTripIndex = k;
          if (nextTripIndex == -1) { // Needed for to choose the right next trip (Get the first expected time as next trip)
            nextTripIndex = k;
          }
        } else if (compareStartTime == 1 && compareEndTimeExtraMins == -1) {
          setUpdateStatusResource = 'finish';
          // currentTripIndex = k;
          nextTripIndex = k + 1; // Do not neede if, because next will be the true one
        }

        await updateEachTripStatusInTripTable(t, setUpdateStatusResource, compareStartTime, compareEndTime); // Update trip status in trip table


      }

      if (!updateStatusResource) {
        if (setUpdateStatusResource) {
          updateStatusResource = setUpdateStatusResource;
        } else {
          let compareNowWithStartTime = currTime.localeCompare(sortedTripsByStartTime[0].start_time);
          let compareNowWithEndTime = currTime.localeCompare(sortedTripsByEndTime[Object.values(sortedTripsByEndTime).length - 1].end_time);

          // Curr time before min time
          if (compareNowWithStartTime == -1) {
            updateStatusResource = 'not_active'
          }

          // Curr time after max time
          if (compareNowWithEndTime == 1) {
            updateStatusResource = 'done'
          }

          // If is somewhere between hours, but not "expected"
          if (!updateStatusResource) {
            updateStatusResource = 'not_active';
          }
        }
      }

      // update current resource status and trip times

      updateCurrentTripId = (currentTripIndex != -1) ? sortedTripsByStartTime[currentTripIndex].id : '';
      updateCurrentTripName = (currentTripIndex != -1) ? sortedTripsByStartTime[currentTripIndex].line_description : '';
      updateCurrentTripStartTime = (currentTripIndex != -1) ? sortedTripsByStartTime[currentTripIndex].start_time : '';
      updateCurrentTripEndTime = (currentTripIndex != -1) ? sortedTripsByStartTime[currentTripIndex].end_time : '';
      updateNextTripId = (nextTripIndex != -1) ? (sortedTripsByStartTime[nextTripIndex] ? sortedTripsByStartTime[nextTripIndex].id : '') : '';
      updateNextTripName = (nextTripIndex != -1) ? (sortedTripsByStartTime[nextTripIndex] ? sortedTripsByStartTime[nextTripIndex].line_description : '') : '';

      await resourcesService.updateResource(
        {
          resource_status: updateStatusResource,
          current_trip_id: updateCurrentTripId,
          current_trip_name: updateCurrentTripName,
          current_trip_start_time: updateCurrentTripStartTime,
          current_trip_end_time: updateCurrentTripEndTime,
          next_trip_id: updateNextTripId,
          next_trip_name: updateNextTripName,
          last_update_trip_data: nowUtc,
        },
        { id: r.id }
      )

    } else {
      // Update the trips status in resources
      await db.resources.update({ resource_status: 'no_trips' }, { where: { id: r.id } })
    }

  } // for

  console.log(`Resources updated`)

}

let cronTask;

// Cron execution settings
if (executeMinutesParam) {
  let executeMinute = parseInt(executeMinutesParam);
  let cronExecuteFormat = '';

  cronExecuteFormat = `*/${executeMinute} * * * *`; // EVERY X MINUTES
  console.log(`Cron will be executed every ${executeMinute} MINUTES`)

  cronTask = cron.schedule(cronExecuteFormat, function () {
    console.log('Cron executed - ', nowUtc)
    updateResourceTripTimes();
  });
}

app.get('/run', (req, res) => {
  res.json(updateResourceTripTimes(req.query.date));
})

app.use('/img', express.static(path.join(__dirname, 'img')));

// db.sequelize.sync({ force: true }).then(function () { // drops the tables
//   // db.sequelize.sync({ alter: true }).then(function () { 
//   console.log("Successfully altered DB");
// });

console.log()
console.log(`Microservice updating trip times for resources on port ${port}`);
app.listen(port);
