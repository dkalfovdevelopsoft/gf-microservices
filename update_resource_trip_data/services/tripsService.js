const { Op } = require("sequelize");

module.exports = (function (db) {
    return {
        addTrip,
        updateTrip,
        getTripTimesForResourceId
    };

    function addTrip(data) {
        return db.trips.create(data)
    }

    function updateTrip(data, where) {
        return db.trips.update(data, { where: where })
    }

    function getTripTimesForResourceId(resource_id) {
        // console.log(`resource_id`, resource_id)
        return db.trips.findAll(
            {
                where: { resource_id: resource_id },
            }
        );
    }
})
